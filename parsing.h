#include <iostream>
#include <sstream>
#include <string>

#ifndef PARSING_H
#define PARSING_H

using namespace std;

class Parsing{
private:
    string namaFile; //nama e-book
    string alamatFile; // alamat dimana e-book ini tersimpan
    string komentarFile; // pesan yang di tinggalkan ketika kita membookmark e-book

public:
    Parsing();

    string getNama(int baris, string isiFile);
    string getAlamat(int baris, string isiFile);
    string getKomentar(int baris, string isiFile);

    int getBarisDariNama(string nama, string isiFile);
    int getBarisDariAlamat(string alamat, string isiFile);
    ~Parsing();
};
#endif //PARSING_H
