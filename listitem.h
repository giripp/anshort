#ifndef LISTITEM_H
#define LISTITEM_H
#include <QListWidgetItem>
#include <iostream>

using namespace std;

class ListItem : QListWidgetItem
{
public:
    ListItem();
    string getFileAddress();
    string getFileComment();
    void setFileAddress(string &);
    void setFileComment(string &);
private:
    string fileAddress;
    string fileComment;
};

#endif // LISTITEM_H
