#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>
#include <QListWidgetItem>
#include <QCheckBox>
#include <QStandardItemModel>
#include "listitem.h"
#include "QDebug"
#include <QFileDialog>

#include <QDesktopServices>
#include <QUrl>
#include <QTimer>

Q_DECLARE_METATYPE(ListItem*)

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    jumlahFile = 0;

    QTimer::singleShot(0, this, SLOT(appReady()));

    indexTerpilih = -1;
}


void MainWindow::appReady(){
    //redrawList();
    qDebug() << "ready";
    redrawList();
}

/**
 * @brief MainWindow::doSomething
 * Memproses file yang diklik pada list
 * @param i
 *
 */
void MainWindow::doSomething(QModelIndex i){
    qDebug() << "open the url";
    /*
     * Ini adalah contoh untuk mendapatkan elemen dari list
     */
    int index = i.row();                        /* Tentukan dulu index terpilih */
    //qDebug() << model->item(index)->data(MyClassRole).value(); /* Tampilkan text dari index terpilih */

    ListItem *li = model->item(index)->data(MyClassRole).value<ListItem*>();
    string fileAddress = li->getFileAddress();
    QString qStrFileAddress = QString::fromStdString(fileAddress);
    qDebug() << qStrFileAddress;

    QString urlFile = "file://";
    urlFile.append(qStrFileAddress);
    QDesktopServices::openUrl(QUrl(urlFile));

}

MainWindow::~MainWindow()
{
    delete ui;
    //delete model;
    delete seqfile;
    delete parsing;
}


QString MainWindow::getNamaFile(QString &fileAddress){
    /*
     * Proses mencari nama file dari alamat file
     */
    int panjangNamaAlamat = fileAddress.length();               //panjang dari alamat file
    int it_pjgAlamat = panjangNamaAlamat;                       //iterator untuk mencari alamat file

    //lakukan komparasi dari belakang hingga ketemu tanda "/"
    while ( QString::compare(fileAddress.mid(it_pjgAlamat, 1),
                             "/",
                             Qt::CaseInsensitive) != 0 ) {
        //qDebug() << fileAddress.mid(it_pjgAlamat, 1);
        it_pjgAlamat--;
    }

    //dapatkan nama file
    QString namaFile = fileAddress.right(panjangNamaAlamat-it_pjgAlamat-1);

    return namaFile;
}


/**
 * @brief MainWindow::redrawList
 * Refresh dari list yang ada
 */
void MainWindow::redrawList(){

    model = new QStandardItemModel(); // tentukan dulu modelnya, model yang menampung data
    /**
     * Kemudian di looping untuk membuat datanya
     */

    string isiFile = seqfile->read();
    int jumlahFile = seqfile->getBanyakBaris();

    qDebug() << jumlahFile;
    for (int i = 0; i < jumlahFile; i++) {

        //QString namaFile = "uhuy";
        QString namaFile = QString::fromStdString(parsing->getNama(i, isiFile));
        string alamatFile = parsing->getAlamat(i, isiFile);
        string komentarFile = parsing->getKomentar(i, isiFile);

        //string fileAddress = "/home/giri/Galau-so.pdf";

        QStandardItem *Item = new QStandardItem();      // ini item yang dimasukkan
        Item->setCheckable( true );                     // apakah bisa dicheck? Tentukan ya
        Item->setCheckState( Qt::Unchecked );           // tentukan status awalnya tidak terpilih
        QString label = QString::number(i, 10);
        label.append(" ");
        label.append(namaFile);                         // tambahkan nama yang ditampilkan di list


        Item->setText(label);                           // Ini adalah nama dari list yang ada
        Item->setEditable(false);                       // supaya list nggak bisa diedit pas diklik dua kali, tentukan false


        //proses biar bisa diklik sesuai dengan filenya
        ListItem *li = new ListItem;


        li->setFileAddress(alamatFile);
        li->setFileComment(komentarFile);
        Item->setData(QVariant::fromValue(li), MyClassRole);;
        model->setItem( i, Item );                      // Dan taruh di dalam model kita
    }

    /**
     * Setelah membuat modelnya, masukkan ke view yang
     * ada di ui kita. Langsung set modelnya, otomatis
     * akan dimasukkan ke dalam view kita
     */
    ui->listView->setModel( model );

    //koneksikan siapa yang akan memproses jika diklik
    connect(ui->listView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(doSomething(QModelIndex)));
}

/**
 * Tombol untuk menambah file
 */
void MainWindow::on_pushButton_clicked()
{
    QString fileAddress = QFileDialog::getOpenFileName(this, tr("Add File"), "", tr("Files (*.*)"));
    qDebug() << fileAddress;

    if (fileAddress.compare("", Qt::CaseInsensitive) != 0) {

        QString fileName = getNamaFile(fileAddress);

        /* mengubah dari QString menjadi std::char
     * !! di windows hal ini tidak berjalan dan harus diubah menjadi
     *    current_locale_text = qs.toLocal8Bit().constData();
     */
        const char *utf8_fa = fileAddress.toUtf8().constData();     //file address tipe char
        string utf8_fa_s = fileAddress.toUtf8().constData();        //file address tipe string




        if (strcmp(utf8_fa, "") == 0) {     //jika tidak ada file dibuka

        }else{                                  //jika ada file yang dibuka

            jumlahFile++;
            //contoh memasukkan data checkbox di QListView

            string masukan;
            masukan.append(fileName.toUtf8().constData());
            masukan.append(",");
            masukan.append(fileAddress.toUtf8().constData());
            masukan.append(",");
            masukan.append(fileName.toUtf8().constData());

            seqfile->add(masukan);
            redrawList();
        }
    }



}

/**
 * @brief MainWindow::on_plainTextEdit_textChanged
 * Saat textnya diubah
 * Langsung saja disimpan
 *
 */
void MainWindow::on_plainTextEdit_textChanged()
{

}

/**
 * @brief MainWindow::on_listView_clicked
 * @param index
 *
 * mendapatkan index dari list
 */
void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    int row = index.row();
    indexTerpilih = index.row();


    /*
     *  Mengubah tampilan comment dari file
     */
    ListItem *li2 = model->item(row)->data(MyClassRole).value<ListItem*>();
    string fileComment = li2->getFileComment();                                  //mendapatkan comment
    QString qStrFileComment = QString::fromStdString(fileComment);

    ui->plainTextEdit->setPlainText(qStrFileComment);
}


/**
 * @brief MainWindow::on_updateBtn_clicked
 * update text file
 */
void MainWindow::on_updateBtn_clicked()
{

    if (indexTerpilih >= 0) {
        ListItem *li = model->item(indexTerpilih)->data(MyClassRole).value<ListItem*>();
        QString fileCommentDariUi = ui->plainTextEdit->toPlainText();    //file comment dari UI

        string utf8_c = fileCommentDariUi.toUtf8().constData();        //file address tipe string

        li->setFileComment(utf8_c);

        string alamat = li->getFileAddress();

        int baris = parsing->getBarisDariAlamat(alamat, seqfile->read());
        string inputNamaFile = parsing->getNama(baris, seqfile->read());
        string inputAlamatFile = parsing->getAlamat(baris, seqfile->read());

        seqfile->editKomentarDenganAlamat(alamat, inputNamaFile, inputAlamatFile, baris, fileCommentDariUi.toUtf8().constData());



    }
    redrawList();

    //reset index yang dipilih
    indexTerpilih = -1;

}

void MainWindow::on_copyToBtn_clicked()
{
    //cek dulu apakah ada yang bisa dicek

    bool adaYangTerpilih = false;

    int j = 0;
    while (adaYangTerpilih == false && j < seqfile->getBanyakBaris()) {
        QStandardItem *it = model->item(j);
        if (it->checkState() == Qt::Checked) {
            adaYangTerpilih = true;
        }
        j++;
    }



    if (adaYangTerpilih) {



        QString dir = QFileDialog::getExistingDirectory(this, tr("Copy To Directory"),
                                                        "",
                                                        QFileDialog::ShowDirsOnly
                                                        | QFileDialog::DontResolveSymlinks);


        for (int i=0; i < seqfile->getBanyakBaris(); i++) {
            QStandardItem *it = model->item(i);
            ListItem *li = it->data(MyClassRole).value<ListItem*>();

            if (it->checkState() == Qt::Checked) {

                QString fileAddress = QString::fromStdString(li->getFileAddress());
                QFile file(fileAddress);

                if (fileAddress.compare("", Qt::CaseInsensitive) != 0) {     //cek ada filenya nggak
                    qDebug() << i;
                    //folder tujuan
                    QString tujuan = dir;

                    //proses alamat tujuan
                    tujuan.append("/");
                    tujuan.append(getNamaFile(fileAddress));

                    //kemudian kita copy
                    file.copy(fileAddress, tujuan);
                }
            }
        }

    }
}

/**
 * @brief MainWindow::on_hapusButton_clicked
 * Tombol untuk menghapus
 */
void MainWindow::on_hapusButton_clicked()
{
    if (indexTerpilih >= 0) {
        ListItem *li = model->item(indexTerpilih)->data(MyClassRole).value<ListItem*>();

        string alamat = li->getFileAddress();

        int baris = parsing->getBarisDariAlamat(alamat, seqfile->read());

        seqfile->del(baris);
        redrawList();

        //tampilan komentar jadi kosong
        ui->plainTextEdit->setPlainText("");
        jumlahFile--;
    }

    //reset index yang dipilih
    indexTerpilih = -1;

}
