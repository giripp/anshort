#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>
#include <QStandardItemModel>
#include "seqfile.h"
#include "parsing.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void doSomething(QModelIndex);
    void on_plainTextEdit_textChanged();
    void on_listView_clicked(const QModelIndex &index);
    void on_updateBtn_clicked();
    void redrawList();
    QString getNamaFile(QString &);

    void on_copyToBtn_clicked();

    void appReady();

    void on_hapusButton_clicked();

private:
    Ui::MainWindow *ui;
    QStandardItemModel *model;

    int jumlahFile;         /* menampung jumlah file yang ada */
    int indexTerpilih;      /* menampung index yang sedang dipilih */
    SeqFile *seqfile;
    Parsing *parsing;

};

enum MyRoles {
    MyClassRole = Qt::UserRole +1
};

#endif // MAINWINDOW_H
