#include <iostream>
#include "parsing.h"
#include <QDebug>
#include <QString>

#ifndef SEQFILE_H
#define SEQFILE_H

using namespace std;

class SeqFile{
private:
    int banyakBaris;
    Parsing *parsing;
public:
    SeqFile();
    string read();
    void add(string isi);
    void del(int baris);
    int getBanyakBaris();
    void editKomentarDenganAlamat(string alamatFile, string inputNamaFile, string inputAlamatFile, int baris, string isiKomentar);
    void init();
    ~SeqFile();
};

#endif

