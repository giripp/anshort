#ifndef FILEUTILITY_H
#define FILEUTILITY_H

#include <QFileInfo>
#include <QString>
#include <QMessageBox>
#include <iostream>
#include <QObject>

using namespace std;

class FileUtility
{
public:
    FileUtility();
    bool copyFile(QString source, QString &destination, bool overwrite, bool move);
};

#endif // FILEUTILITY_H
