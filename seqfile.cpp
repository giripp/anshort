#include <iostream>
#include <string.h>
#include <fstream>
#include <stdio.h>
#include "SeqFile.h"
#include <QString>

using namespace std;


SeqFile::SeqFile(){
}


//untuk menambahkan data ke dalam file
string SeqFile::read(){
    char buffer[10000];
    //string buffer2[10000];
    string hasilReturn;
    int i = 0;
    fstream file ("InfoFile.txt", ios::in);
    while(!file.eof()){
        file.getline(buffer,1000);
        //buffer2[i] = buffer;
        hasilReturn.append(buffer);
        hasilReturn.append("\n");
        //cout << buffer2[i] << endl;
        i++;
    }

    //file << "punya_doni.a3gp" << endl;
    file.close();

    return hasilReturn;
}

//untuk menambahkan data ke dalam file
void SeqFile::add(string isi){

    char buffer[1000];
    string buffer2[1000];
    int i = 0;
    fstream file ("InfoFile.txt", ios::in);
    while(!file.eof()){
        file.getline(buffer,1000);
        buffer2[i] = buffer;
        i++;
    }

    file.close();

    fstream file_output ("InfoFile.txt", ios::out);
    for(int j=0;j<i-1;j++){
        file_output << buffer2[j] << "\n";
    }
    file_output << isi << "\n";

    file_output.close();
}

//untuk menghapus data di dalam file
void SeqFile::del(int baris){
    char buffer[10000];
    string buffer2[10000];
    int i = 0;
    fstream file ("InfoFile.txt", ios::in);

    //tampung dulu filenya
    while(!file.eof()){
        file.getline(buffer,1000);
        buffer2[i] = buffer;
        i++;
    }

    file.close();

    fstream file_output ("InfoFile.txt", ios::out);

    //menghapus file
    for(int j=0;j<i-1;j++){
        if(j!=baris){
            file_output << buffer2[j] << endl;
            cout << buffer2[j] << endl;
        }
    }

    file_output.close();

}

int SeqFile::getBanyakBaris(){
    char buffer[10000];
    string buffer2[10000];
    int i = 0;
    fstream file ("InfoFile.txt", ios::in);

    //cari banyak file
    while(!file.eof()){
        file.getline(buffer,1000);
        buffer2[i] = buffer;
        i++;
    }
    file.close();

    return i-1;
}

SeqFile::~SeqFile(){
}

void SeqFile::init(){
    fstream file_output ("InfoFile.txt", ios::out);

    file_output << "filename,filepos,komentar"<<endl;

    file_output.close();
}

void SeqFile::editKomentarDenganAlamat(string alamatFile, string inputNamaFile, string inputAlamatFile, int baris, string isiKomentar){

    for (int i = 0; i < isiKomentar.length(); i++) {
        if (isiKomentar[i] == '\n') {
            isiKomentar[i] = ' ';
        }
    }

    string isiFile = SeqFile::read();


    qDebug() << QString::fromStdString(isiFile);
    qDebug() << QString::fromStdString(alamatFile);
    //qDebug() << parsing->getBarisDariAlamat(alamatFile, isiFile);


    //string komentarFile = parsing->getKomentar(baris, isiFile);




    string output;
    output.append(inputNamaFile);
    output.append(",");
    output.append(inputAlamatFile);
    output.append(",");
    output.append(isiKomentar);

    char buffer[1000];
    string buffer2[1000];
    int i = 0;

    fstream file ("InfoFile.txt", ios::in);

    //tampung dulu filenya
    while(!file.eof()){
        file.getline(buffer,1000);
        buffer2[i] = buffer;
        i++;
        qDebug() << QString::fromStdString(buffer2[i]);
    }

    file.close();
    fstream file_output ("InfoFile.txt", ios::out);

    for(int j=0;j<i-1;j++){
        if(j!=baris){
            file_output << buffer2[j] << endl;
        }else{
            file_output << output << endl;
        }
    }

    file_output.close();

}
