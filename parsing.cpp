#include "Parsing.h"

Parsing::Parsing(){
}

Parsing::~Parsing(){
}

string Parsing::getNama(int baris, string isiFile){
    unsigned int index = 0;
    int currBaris = 0; //current baris, baris yang lagi dicek
    string kosong = "";

    while (index < isiFile.length()-1) {

        string namaFile;
        string alamatFile;
        string komentarFile;

        //mencari nama file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {

            stringstream ss;
            string s;

            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            namaFile.append(s);
            index++;
        }
        index++;

        /**
         *	Lemparkan keluaran hasil jika baris
         *  yang sedang dicek sesuai dengan pemeriksaan
         */
        if (baris == currBaris) {
            return namaFile;
        }



        //mencari alamat file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }
            alamatFile.append(s);
            index++;
        }

        index++;

        //mencari komentar file (sampai ketemu '\n')
        while (index < isiFile.length() &&
               isiFile[index] != '\n' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            komentarFile.append(s);
            index++;
        }

        currBaris++;	//cek ke baris berikutnya
    }

    return kosong;
}

string Parsing::getAlamat(int baris, string isiFile){
    unsigned int index = 0;
    int currBaris = 0; //current baris, baris yang lagi dicek
    string kosong = "";

    while (index < isiFile.length()-1) {

        string namaFile;
        string alamatFile;
        string komentarFile;

        //mencari nama file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {

            stringstream ss;
            string s;

            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            namaFile.append(s);
            index++;
        }

        index++;

        //mencari alamat file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }
            alamatFile.append(s);
            index++;
        }

        index++;
        /**
         *	Lemparkan keluaran hasil jika baris
         *  yang sedang dicek sesuai dengan pemeriksaan
         */
        if (baris == currBaris) {
            return alamatFile;
        }

        //mencari komentar file (sampai ketemu '\n')
        while (index < isiFile.length() &&
               isiFile[index] != '\n' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            komentarFile.append(s);
            index++;
        }

        currBaris++;	//cek ke baris berikutnya
    }
    return kosong;
}


string Parsing::getKomentar(int baris, string isiFile){
    unsigned int index = 0;
    int currBaris = 0; //current baris, baris yang lagi dicek
    string kosong = "";

    while (index < isiFile.length()-1) {

        string namaFile;
        string alamatFile;
        string komentarFile;

        //mencari nama file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {

            stringstream ss;
            string s;

            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            namaFile.append(s);
            index++;
        }

        index++;

        //mencari alamat file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }
            alamatFile.append(s);
            index++;
        }

        index++;


        //mencari komentar file (sampai ketemu '\n')
        while (index < isiFile.length() &&
               isiFile[index] != '\n' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            komentarFile.append(s);
            index++;

        }

        /**
         *	Lemparkan keluaran hasil jika baris
         *  yang sedang dicek sesuai dengan pemeriksaan
         */
        if (baris == currBaris) {
            return komentarFile;
        }

        currBaris++;	//cek ke baris berikutnya


    }
    return kosong;
}

int Parsing::getBarisDariNama(string nama, string isiFile){
    unsigned int index = 0;
    int currBaris = 0; //current baris, baris yang lagi dicek

    while (index < isiFile.length()-1) {

        string namaFile;
        string alamatFile;
        string komentarFile;

        //mencari nama file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {

            stringstream ss;
            string s;

            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            namaFile.append(s);
            index++;
        }

        if (namaFile.compare(nama) == 0) {
            return currBaris;
        }

        index++;

        //mencari alamat file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }
            alamatFile.append(s);
            index++;
        }

        index++;


        //mencari komentar file (sampai ketemu '\n')
        while (index < isiFile.length() &&
               isiFile[index] != '\n' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            komentarFile.append(s);
            index++;

        }

        currBaris++;	//cek ke baris berikutnya

    }

    //melemparkan nilai -1 jika tidak ketemu apa-apa
    return -1;
}

int Parsing::getBarisDariAlamat(string alamat, string isiFile){
    unsigned int index = 0;
    int currBaris = 0; //current baris, baris yang lagi dicek

    while (index < isiFile.length()-1) {

        string namaFile;
        string alamatFile;
        string komentarFile;

        //mencari nama file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {

            stringstream ss;
            string s;

            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            namaFile.append(s);
            index++;
        }

        index++;

        //mencari alamat file (sampai ketemu ',')
        while (index < isiFile.length() &&
               isiFile[index] != ',' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }
            alamatFile.append(s);
            index++;
        }

        if (alamatFile.compare(alamat) == 0) {
            return currBaris;
        }

        index++;


        //mencari komentar file (sampai ketemu '\n')
        while (index < isiFile.length() &&
               isiFile[index] != '\n' ) {
            stringstream ss;
            string s;
            if (isiFile[index] == ' ') {
                s = " ";
            }else{
                ss << isiFile[index];
                ss >> s;
            }

            komentarFile.append(s);
            index++;

        }

        currBaris++;	//cek ke baris berikutnya

    }

    //melemparkan nilai -1 jika tidak ketemu apa-apa
    return -1;
}
