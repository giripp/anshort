#-------------------------------------------------
#
# Project created by QtCreator 2012-12-16T11:13:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Anshort
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    listitem.cpp \
    fileutility.cpp \
    seqfile.cpp \
    parsing.cpp

HEADERS  += mainwindow.h \
    listitem.h \
    fileutility.h \
    seqfile.h \
    parsing.h

FORMS    += mainwindow.ui
